---
title: sau vài ngày mưa
date: "2022-06-14"
categories:
- raw-thought
tags: 
- quick-note
---

trời sau mưa rất mát. cuối cùng tháng sáu đã thấp thoáng một chút màu xanh của bầu trời mùa hè. có hai bài hát rất hợp trong không khí này, một ngày hè-nhưng-chưa-hẳn-hè, Có một mùa hè của Phạm Toàn Thắng và Hoàng Diệu của Những đứa trẻ. Khánh rất thương (hoặc thấy tội tội) tụi Những đứa trẻ. mấy bạn đó cù bơ cù bất mỗi đứa một việc tè le gì đó, gọi là lông bông, và nghèo. nhớ hồi qua HRC mấy tuần trước tham gia buổi ra mắt album band gì đó hổng nhớ tên, band đó có hai ông chú, chú già nhưng outfit không già, chú còn lại trông có vẻ già và outfit thì như nhân viên văn phòng mới tan làm về đi chơi nhạc. sau đợt đó tự nhủ: chà để sẵn tâm thế chơi nhạc after work sẽ mặc đồ trông như người bình thường đi chơi nhạc - để tụi trẻ con (hoặc khán giả) không cười cười chỉ trỏ: ê bà chơi guitar kia vừa đi làm về chạy ra chơi nhạc kìa mày. kiểu vậy.

viết xuống thật tốt. bởi vì khi đầu óc có quá nhiều thứ nghĩ suy về, thì viết xuống là một cách gỡ rối những mớ suy nghĩ đó. ngoài ra vì đầu óc không nhớ nổi sự kiện diễn ra mà chỉ có thể được mang máng gợi nhắc về cảm giác lúc đó, viết xuống là một hình thức giúp nhớ về sự kiện, và đâu đó, cũng giúp viết lại quá khứ. hẳn là. vì đâu còn nhớ gì, những việc từng coi tồn tại giờ chỉ còn trên một trang giấy, hoặc, trong thời buổi này, là trên một màn hình, một trang blog này đây. vì vậy, việc lựa chọn chuyện gì để viết lại, để sau này nhớ về là quan trọng. có thể lựa chọn viết lời phàn nàn, hoặc giả có thể lựa chọn viết về những điều tốt đẹp vừa xảy ra trong giây phút ngắn ngủi từng ngày qua. có một luật nhỏ xíu, gọi là "little things that matter", những điều nhỏ mà có võ. nhặt nhạnh từng điều nhỏ xíu gom vô một chai thời gian. đến cuối ngày, hay gần cuối một cuộc đời người, mở ra hít ngửi để nhớ lại (dù không đúng lắm, hoặc đúng?) những khoảnh khắc nhỏ xíu đó chắc là khá tuyệt.

riết rồi nhớ về một trang blog của người thương phương xa, cách nhau đâu mấy kilomets nhưng trái tim xa tận chân trời không tài nào chạm tới. có đôi lúc chật vật mù mắt loạng quạng khắp chốn chỉ muốn trở lại nơi đó tìm một điểm tựa để vững chân. mà không còn.

ngày mai hay ngày kia hay nhiều ngày nữa nếu còn chạng vạng mù quáng không tìm được đâu thì dành nơi này thay cho điểm tựa đó. hẳn là chưa bằng. thời gian trôi qua bồi đắp dần. là một điểm tựa tốt, sớm thôi.