---
title: This will be our year
date: "2021-12-12"
categories:
- raw-thought
tags:
- mics
- free-writing
---

This post will be short. I write because there is an urge talks to me that I should write. 

"This will be our year" is a song by The Zombies, also a title of my spotify playlist. The playlist's vibe is kind of wistful. I don't know this word, until in Spotify's 2021 review, it told me my audio aura was "chill and wistful". I guess it's true.

"Grow old with me" by John Lennon is also in this playlist. I love that song and listen to it regularly. I still can't get it, but listening this song reminds me of Chau. And when I think of him, I remember that "I hope the life you live with full of love that you want".

> Grow old along with me
>
> Two branches of one tree
>
> Face the setting sun
>
> When the day is done
>
> God bless our love

I've learned a new word which describes my life in these recent months, "humdrum". Have to admit that learning vocabulary is fun, especially with the assistance of a languange (kinda) master. I like the way he tried to explain and connect the new words with familiar concepts/memories such that I can remember. It's fun, and a once-at-a-lifetime experience. This connection and comfortability might not exist the next time in my life.

The following picture also portraits me.

![cat tumblr](/cat-tumblr.jpeg)