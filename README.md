Basic tasks with Hugo
---

Create new post with draft status:
```
title: "My first post"
date: 2020-06-17
draft: true
categories:
- raw-thought
tags: 
- best-practice
- raw-thought
- ocd-style
```

Start the Hugo server: `hugo server -D` (enable Draft)

Build static page: `hugo -D`

After building static page, the output will be in `./public/` directory by default. Drafts do not get deployed; once you finish a post, update header of the post to say `draft: false`

[Quick start of Hugo](https://gohugo.io/getting-started/quick-start/)

To learn how to embed media using shortcodes: [How to use <iframe> correctly in yaml file](https://discourse.gohugo.io/t/how-to-use-iframe-content-correctly-in-a-yaml-file/14360/3)